# RELEASE NOTES: *yaTree*, Yet Another Tree - list contents of directories in a tree-like format

- **Version 1.4.11**:
  - Updated build system components.

- **Version 1.4.10**:
  - Updated build system.

- **Version 1.4.9**:
  - Removed unused files.

- **Version 1.4.8**:
  - Updated build system component(s)

- **Version 1.4.7**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.4.6**:
  - Some minor changes in .comment file(s).

- **Version 1.4.5**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.4.4**:
  - Fixed ./Makefile and ./src/Makefile.
  - Removed compilation warning in ./src/yaTree.c.

- **Version 1.4.3:**
  - Added tagging of new release.

- **Version 1.4.2:**
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Moved from GPL v2 to GPL v3.
  - Replaced text files by markdown version: RELASENOTES, COPYING, LICENSE.

- **Version 1.4.1:**
    - Fixed typos in README.md file.

- **Version 1.4.0:**
  -Removed HTML short description for GitHub ('index.html' file). Was a mistake; the README file do very well the job.
  -Improved "clean" target in str/Makefile.
  -Formatting and improving the README file.

- **Version 1.3.0:**
  -Some naming modification according to 'yaComment'.
  -Fixed typos and bad links.

- **Version 1.2.2:**
  -Remove 'rewinddir' workaround. Bug fixed in Windows 10 Home Insider Preview. Build 16278.rs3_release.170825-1441

- **Version 1.2.1:**
  -Issue on Ubuntu subsystem for Windows (lxss): the 'rewindir' fails. Workaround implemented.

- **Version 1.2.0:**
  -Added 'yaComment' bash script: '.comment' files generator
- **Version 1.1.0:**
  -Added .comments file management. If a .comment file exists in a directory, we append the content (comment from each file and directory) to the output of the files and directories.
- **Version 1.0.3:**
  -Processed accented letters for the calculation of the length of the lines to be printed
  -Fix bug in compare function (src/yatree.c)
  -Minor fix in Makefiles

- **Version 1.0.2:**
  -Fixed typos and omission in README file.

- **Version 1.0.1:**
  -Introduced cross generation for Windows (make wall|wclean|winstall|wcleaninstall).
  -Fix: symbolic links not displayed. Use "lstat" instead of "stat".

- **Version 1.0.0:**
  -First release.
