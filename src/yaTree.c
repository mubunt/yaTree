//------------------------------------------------------------------------------
// Copyright (c) 2017-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaTree
// Yet Another Tree - list contents of directories in a tree-like format
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <sys/stat.h>
#ifdef LINUX
#include <linux/limits.h>
#include <unistd.h>
#else
#include <limits.h>
#endif
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "yaTree_cmdline.h"
#include "gPrint.h"
#include "common.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define COMMENT 		'#'
#define COMMENTFILE		".comment"
#define TOPLEVELCOMMENT	"Application level"
#ifndef LINUX
#define	lstat 			stat
#endif
#define verbose(fmt, ...) \
	do { if (args_info.verbose_given) fprintf(stderr, "[VERBOSE] " fmt "\n", __VA_ARGS__); } while (0)
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static char 						startdir[PATH_MAX];
static size_t 						totalNumberDirs = 0;
static size_t						totalNumberFiles = 0;
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static int compare(void const *a, void const *b) {
	return strcmp(a, b);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static boolean existFile(char *file) {
	struct stat locstat;
	if (file == NULL) return FALSE;
	if (stat(file, &locstat) < 0) return FALSE;
	return TRUE;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void printStatus(const char *fmt, size_t nbd, size_t nbf) {
	fprintf(stdout, "\n");
	fprintf(stdout, fmt, nbd, nbf);
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static size_t computeIndex(size_t maxlen, size_t idx) {
	return (idx * (maxlen + 1));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *getObject(char *pt, size_t maxlen, size_t idx) {
	return pt + computeIndex(maxlen, idx);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static boolean lookForObject(char *name,
                             char *ptdir, size_t nbdir, size_t maxdirlen,
                             char *ptfile, size_t nbfile, size_t maxfilelen,
                             size_t *index) {
	for (size_t i = 0; i < nbdir; i++) {
		int k = strcmp(ptdir + computeIndex(maxdirlen, i), name);
		if (k == 0) {
			*index = i;
			return TRUE;
		}
		if (k > 0) break;
	}
	for (size_t i = 0; i < nbfile; i++) {
		int k = strncmp(ptfile + computeIndex(maxfilelen, i), name, strlen(name));
		if (k == 0) {
			*index = nbdir + i;
			return TRUE;
		};
		if (k > 0) break;
	}
	return FALSE;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static boolean readCommentFile(char *filename,
                               char *ptdir, size_t nbdir, size_t maxdirLen,
                               char *ptfile, size_t nbfile, size_t maxfilelen,
                               char *ptdesc, size_t nbdesc, size_t maxdesclen) {
	FILE *file;
	if (NULL == (file = fopen(filename, "r"))) {
		error("Cannot open file '%s'", filename);
		return FALSE;
	}
	for (size_t i = 0; i < nbdesc; i++) strcpy(ptdesc + computeIndex(maxdesclen, i), "");
	char line[MAX_DESCR + 1];
	size_t index;
	boolean found = FALSE;
	int l = 0;
	while (fgets(line, sizeof(line), file)) {
		++l;
		if (line[strlen(line) - 1] == '\n') line[strlen(line) - 1] = '\0';
		if (line[0] == '\0') continue;
		if (line[0] == COMMENT) continue;
		int i = 0;
		if (line[0] == ' ' || line[0] == '\t') { // Continuation of the previous comment
			if (! found) {
				error("Syntax error in file '%s', line %d: No file name", filename, l);
				fclose(file);
				return FALSE;
			}
			while (line[i] == ' ' || line[i] == '\t') ++i;
			strncat(ptdesc + computeIndex(maxdesclen, index), CONTINUATION_LINE, maxdesclen);
			strncat(ptdesc + computeIndex(maxdesclen, index), line + i, maxdesclen);
			continue;
		}
		while (line[i] != ' ' && line[i] != '\t' && line[i] != '\0') ++i;
		if ((size_t)i == strlen(line)) continue;	// No comment area
		line[i] = '\0';
		if ((found = lookForObject(line, ptdir, nbdir, maxdirLen, ptfile, nbfile, maxfilelen, &index))) {
			++i;
			while (line[i] == ' ' || line[i] == '\t') ++i;
			strncpy(ptdesc + computeIndex(maxdesclen, index), line + i, maxdesclen);
		}
	}
	fclose(file);
	return TRUE;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void processFile(char *fname, char *object, mode_t mode, char *res) {
	char s[PATH_MAX];
#ifdef LINUX
	char ss[PATH_MAX * 2];
	char slink[PATH_MAX];
	ssize_t len;
#endif
	strcpy(res, fname);
	switch (mode) {
	case S_IFBLK:
		strcat(res, " [block device]");
		break;
	case S_IFCHR:
		strcat(res, " [character device]");
		break;
	case S_IFIFO:
		strcat(res, " [FIFO]");
		break;
#ifdef LINUX
	case S_IFLNK:
		if ((len=readlink(object, slink, sizeof(slink))) < 0)
			strcat(s, " [error reading symbolic link information]");
		else {
			slink[len] = 0;
			sprintf(ss, " [-> %s]", slink);
			strcat(res, ss);
		}
		break;
	case S_IFSOCK:
		strcat(res, " [socket]");
		break;
#endif
	case S_IFREG:
		break;
	default:
		strcat(res, " [UNKNOWN]");
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void processTree(size_t level, char *directory) {
	DIR *pDir;
	struct dirent *pDirent;
	struct stat sb;
	char object[PATH_MAX];

	if ((pDir = opendir(directory))  == NULL) {
		error("Cannot open directory '%s'", directory);
		return;
	}
	verbose("Level: %s", directory);

	// Step 1: Count the number of directories and regular files.
	size_t nbDirs = 0;
	size_t nbFiles = 0;
	size_t maxDirLen = 0;
	size_t maxFileLen = 0;
	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (strncmp(pDirent->d_name, ".", 2) == 0) continue;
		if (strncmp(pDirent->d_name, "..", 3) == 0) continue;
		if (pDirent->d_name[0] == '.' && ! args_info.all_given) continue;
		sprintf(object, "%s/%s", directory, pDirent->d_name);
		if (lstat(object, &sb) < 0)  {
			error("Cannot stat %s\n", object);
			continue;
		}
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFDIR) {
			++nbDirs;
			maxDirLen = max(maxDirLen, strlen(pDirent->d_name));
			continue;
		}
		char s[PATH_MAX];
		processFile(pDirent->d_name, object, mode, s);
		++nbFiles;
		maxFileLen = max(maxFileLen, strlen(s));
	}
#ifdef LINUX
	verbose("\tNumber of directories: %ld - Length of longest name: %ld.", nbDirs, maxDirLen);
	verbose("\tNumber of regular files: %ld - Length of longest name: %ld.", nbFiles, maxFileLen);
#else
	verbose("\tNumber of directories: %I64u - Length of longest name: %I64u.", nbDirs, maxDirLen);
	verbose("\tNumber of regular files: %I64u - Length of longest name: %I64u.", nbFiles, maxFileLen);
#endif

	// Step 2: Allocate table for directories and regular files and fill them.
	char *ptDirs;
	char *ptFiles;
	if (NULL == (ptDirs = (char *)calloc(nbDirs, maxDirLen + 1))) {
		error("%s", "Cannot allocate memory for directories");
		closedir(pDir);
		return;
	}
	if (NULL == (ptFiles = (char *)calloc(nbFiles, maxFileLen + 1))) {
		error("%s", "Cannot allocate memory for regular files");
		free(ptDirs);
		closedir(pDir);
		return;
	}

	rewinddir(pDir);
	//closedir(pDir);													// WORKAROUND:
	//if ((pDir = opendir(directory))  == NULL) {						// On Ubuntu subsystem for Windows (lxss),
	//	error("Cannot open (second time) directory '%s'", directory);	// the 'rewindir' fails.
	//	return;															//		Description:Ubuntu 16.04.3 LTS
	//}																	//		Release:	16.04
	//		Codename:	xenial

	size_t indDirs = 0;
	size_t intFiles = 0;
	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (strncmp(pDirent->d_name, ".", 2) == 0) continue;
		if (strncmp(pDirent->d_name, "..", 3) == 0) continue;
		if (pDirent->d_name[0] == '.' && ! args_info.all_given) continue;
		sprintf(object, "%s/%s", directory, pDirent->d_name);
		if (lstat(object, &sb) < 0)  continue;
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFDIR) {
			strcpy(ptDirs + computeIndex(maxDirLen, indDirs), pDirent->d_name);
			++indDirs;
		} else {
			char s[PATH_MAX];
			processFile(pDirent->d_name, object, mode, s);
			strcpy(ptFiles + computeIndex(maxFileLen, intFiles), s);
			++intFiles;
		}
	}
	closedir(pDir);

	// Step 3: Sort these tables alphabetically and complete them with a description of each element
	//         if a COMMENTFILE file exists.
	char *ptDescription = NULL;
	qsort(ptDirs, nbDirs, maxDirLen + 1, compare);
	qsort(ptFiles, nbFiles, maxFileLen + 1, compare);
	if (! args_info.nodescription_given) {
		sprintf(object, "%s/%s", directory, (char *)COMMENTFILE);
		if (existFile(object)) {
			if (NULL == (ptDescription = (char *)calloc(nbDirs + nbFiles, MAX_DESCR + 1))) {
				error("%s", "Cannot allocate memory for description");
			} else {
				if (! readCommentFile((char *)object,
				                      ptDirs, nbDirs, maxDirLen,
				                      ptFiles, nbFiles, maxFileLen,
				                      ptDescription, nbDirs + nbFiles, MAX_DESCR)) {
					free(ptDescription);
					ptDescription = NULL;
				}
			}
		}
	}
	verbose("\tFile %s: %s", COMMENTFILE, (ptDescription == NULL) ? "no" : "yes");

	// Step 4: Display these tables. Call for the next levels.
	for (size_t i = 0; i < nbDirs; ++i) {
		if (ptDescription == NULL)
			gprint_addDir(level, (i == (nbDirs - 1) && nbFiles == 0), getObject(ptDirs, maxDirLen, i), (char *)"");
		else
			gprint_addDir(level, (i == (nbDirs - 1) && nbFiles == 0), getObject(ptDirs, maxDirLen, i), getObject(ptDescription, MAX_DESCR, i));
		sprintf(object, "%s/%s", directory, getObject(ptDirs, maxDirLen, i));
		processTree(level + 1, object);
	}

	if (! args_info.directory_given) {
		for (size_t i = 0; i < nbFiles; ++i) {
			if (ptDescription == NULL)
				gprint_addFile(level, (i == (nbFiles - 1)), getObject(ptFiles, maxFileLen, i), (char *)"");
			else
				gprint_addFile(level, (i == (nbFiles - 1)), getObject(ptFiles, maxFileLen, i), getObject(ptDescription, MAX_DESCR, nbDirs + i));
		}
	}

	totalNumberDirs += nbDirs;
	totalNumberFiles += nbFiles;
	if (ptDescription != NULL) free(ptDescription);
	free(ptDirs);
	free(ptFiles);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat locstat;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_yaTree(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	if (args_info.inputs_num > 1) {
		error("Usage: %s [OPTIONS]... DIRECTORY\n", argv[0]);
		goto Error;
	}
	if (args_info.inputs_num == 0)
		strcpy(startdir, ".");
	else
		strcpy(startdir, args_info.inputs[0]);
	if (startdir[strlen(startdir) - 1] == '/') startdir[strlen(startdir) - 1] = '\0';
	//---- Check startdir directory ---------------------------------------------
	if (stat(startdir, &locstat) < 0)  {
		error("Cannot stat %s\n", startdir);
		goto Error;
	}
	if (! S_ISDIR(locstat.st_mode)) {
		error("%s is not a directory\n", startdir);
		goto Error;
	}
	//---- Summary --------------------------------------------------------------
	verbose("%s", "Options:");
	verbose("\tall files: %s", args_info.all_given ? "yes": "no");
	verbose("\tonly directory: %s", args_info.directory_given ? "yes": "no");
	verbose("\tturn off file/directory count: %s", args_info.noreport_given ? "yes": "no");
	verbose("\tstart directory: %s", startdir);
	//--- Processing - Step 1 --------------------------------------------------
	gprint_ini();
	if (args_info.nodescription_given)
		gprint_addDir(0, false, startdir, (char *)"");
	else
		gprint_addDir(0, false, startdir, (char *)TOPLEVELCOMMENT);
	processTree(1, startdir);
	gprint_close(stdout);
	if (! args_info.noreport_given)
#ifdef LINUX
		printStatus("%ld directories, %ld files", totalNumberDirs, totalNumberFiles);
#else
		printStatus("%I64u directories, %I64u files", totalNumberDirs, totalNumberFiles);
#endif
	//--- Exit -----------------------------------------------------------------
	cmdline_parser_yaTree_free(&args_info);
	return EXIT_SUCCESS;
Error:
	cmdline_parser_yaTree_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
