//------------------------------------------------------------------------------
// Copyright (c) 2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaTree
// Yet Another Tree - list contents of directories in a tree-like format
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#ifdef LINUX
#include <sys/ioctl.h>
#endif
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include "gPrint.h"
#include "common.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define TERM						"TERM"
#define MSYSCON						"MSYSCON"

// XTERM-LIKE GRAPHIC CHARSET
#define XGRAPHMODEON				"\033(0"
#define XGRAPHMODEOFF				"\033(B"
#define XSLBC						109	// Single Left Bottom Corner
#define	XSH							113	// Single Horizontal
#define XSVBR						116	// Single Vertical Bar Right
#define XSV							120	// Single Vertical
// WINDOWS CMD GRAPHIC CHARSET
// https://en.wikipedia.org/wiki/Code_page_437
#define SV							179	// Single Vertical
#define SLBC						192	// Single Left Bottom Corner
#define SVBR						195	// Single Vertical Bar Right
#define	SH							196	// Single Horizontal

#define COMMENT						"# "
#define TMPFILE_FILE				"__yaTreeFile"
#define TMPFILE_COMMENT				"__yaTreeComment"
#define LEVELMAX					128

#define fatal(fmt, ...) 			error(fmt, __VA_ARGS__); gprint_abort(); exit(-1);
//------------------------------------------------------------------------------
// ENUMERATION
//------------------------------------------------------------------------------
typedef enum {
	XTERM,
	XTERMWIN,
	CMDWIN
} eTerminalType;
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static FILE 			*__fdfile		= NULL;
static FILE 			*__fdcomment	= NULL;
static char				tmpnameFile[64];
static char				tmpnameComment[64];
static size_t			__width			= 0;
static size_t			__currentWidth 	= 0;
static eTerminalType	__terminalType	= XTERM;
static bool 			bar[LEVELMAX];
//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static eTerminalType __gprint_getTerminalType() {
	char *pt;

	if (NULL == getenv(TERM))					// Windows Command Line
		return(CMDWIN);
	if ((pt = getenv(MSYSCON)) == NULL)			// Linux shell
		return(XTERM);
	if (strcmp(pt, "sh.exe") == 0)				// Windows/Bash shell
		return(CMDWIN);
	return(XTERMWIN);
}
//-----------------------------------------------------------------------------
void __gprint_line(FILE *fdout, size_t width, char *buff, char *comment, size_t widthC, unsigned char *indent) {
	if (width != 0) {
		fprintf(fdout, "%s", buff);
		for (size_t i = width; i < __width; i++) fprintf(stdout, " ");
		fprintf(stdout, " %s", COMMENT);
	}
	char *token = strtok(comment, CONTINUATION_LINE);
	if (token == NULL)
		fprintf(fdout, "\n");
	else {
		fprintf(fdout, "%s\n", token);
		token = strtok(NULL, CONTINUATION_LINE);
		while (token != NULL) {
			fprintf(stdout, "%s", indent);
			for (size_t i = widthC; i < __width; i++) fprintf(stdout, " ");
			fprintf(stdout, " %s%s\n", COMMENT, token);
			token = strtok(NULL, CONTINUATION_LINE);
		}
	}
}
//-----------------------------------------------------------------------------
void __gprint_add(const char *format, char *info) {
	char buff[2048];
	size_t endoflineIndex, tobesubstrated;

	if (__fdfile == NULL || __fdcomment == NULL) {
		fatal("%s", "'gprint' package not initialized");
	}

	sprintf(buff, format, info);
	size_t n = fwrite(buff, 1, strlen(buff), __fdfile);
	char *pt = buff;
	endoflineIndex = tobesubstrated = 0;
	for (size_t i = 0; i < n; i++) {
		if (buff[i] == '\n') {
			endoflineIndex = i;
			buff[i] = '\0';
			__width = max(__width, (__currentWidth + strlen(pt) - tobesubstrated));
			tobesubstrated = __currentWidth = 0;
			pt = buff + i + 1;
		} else {
			if ((__terminalType == XTERM) || (__terminalType == XTERMWIN)) {
				char *ptmp;
				ptmp = buff + i;
				if ((strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) ||
				        (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0))
					tobesubstrated += strlen(XGRAPHMODEON);
			}
		}
	}
	if (endoflineIndex != n - 1) __currentWidth += n - endoflineIndex - tobesubstrated;
}
//-----------------------------------------------------------------------------
void __gprint_addComment(const char *format, char *comment) {
	char buff[2048];
	if (__fdfile == NULL || __fdcomment == NULL) {
		fatal("%s", "'gprint' package not initialized");
	}
	sprintf(buff, format, comment);
	fwrite(buff, 1, strlen(buff), __fdcomment);
}
//------------------------------------------------------------------------------
void __gprint_addIndent(size_t level, bool last) {
	if (__fdfile == NULL || __fdcomment == NULL) {
		fatal("%s", "'gprint' package not initialized");
	}
	if (level == 0) return;
	if (level >= LEVELMAX) {
#ifdef LINUX
		fatal("Too many levels of diretories (%ld)", level);
#else
		fatal("Too many levels of diretories (%I64u)", level);
#endif
	}
	bar[level - 1] = last;
	char buff[2048];
	unsigned char s[5];
	switch (__terminalType) {
	case CMDWIN:		// Windows command line and Windows/Bash
		buff[0] = '\0';
		s[0] = SV;
		s[1] = s[2] = s[3] = ' ';
		s[4] = '\0';
		for (size_t i = 0; i < (level - 1); ++i) {
			if (bar[i]) strcat(buff, "    ");
			else strcat(buff, (char *)s);
		}
		if (last) s[0] = SLBC;
		else s[0] = SVBR;
		s[1] = s[2] = SH;
		s[3] = ' ';
		s[4] = '\0';
		strcat(buff, (char *)s);
		break;
	case XTERM:			// Linux (XTERM)
	case XTERMWIN:		// and MinGW shell
		strcpy(buff, XGRAPHMODEON);
		s[0] = XSV;
		s[1] = s[2] = s[3] = ' ';
		s[4] = '\0';
		for (size_t i = 0; i < (level - 1); ++i) {
			if (bar[i]) strcat(buff, "    ");
			else strcat(buff, (char *)s);
		}
		if (last) s[0] = XSLBC;
		else s[0] = XSVBR;
		s[1] = s[2] = XSH;
		s[3] = ' ';
		s[4] = '\0';
		strcat(buff, (char *)s);
		strcat(buff, XGRAPHMODEOFF);
		break;
	}
	__gprint_add("%s", buff);
}
//------------------------------------------------------------------------------
// GPRINT_INI
//------------------------------------------------------------------------------
void gprint_ini() {
	// Type of terminal (xterm, xterm-windows, cmd-windows)
	__terminalType = __gprint_getTerminalType();
	// Temporary file creation
#ifdef LINUX
	sprintf(tmpnameFile, "%s/%s%d", P_tmpdir, TMPFILE_FILE, getpid());
	sprintf(tmpnameComment, "%s/%s%d", P_tmpdir, TMPFILE_COMMENT, getpid());
#else
	sprintf(tmpnameFile, "%s/%s%d", "/tmp", TMPFILE_FILE, getpid());
	sprintf(tmpnameComment, "%s/%s%d", "/tmp", TMPFILE_COMMENT, getpid());
#endif
	if (NULL == (__fdfile = fopen(tmpnameFile, "w+"))) {
		fatal("%s", strerror(errno));
	}
	if (NULL == (__fdcomment = fopen(tmpnameComment, "w+"))) {
		(void) fclose(__fdfile);
		(void)unlink(tmpnameFile);
		fatal("%s", strerror(errno));
	}
	// Some initializations
	__currentWidth = __width = 0;
}
//------------------------------------------------------------------------------
// GPRINT_ADDDIR
//------------------------------------------------------------------------------
void gprint_addDir(size_t level, bool last, char *object, char *comment) {
	__gprint_addIndent(level, last);
	__gprint_add("%s/\n", object);
	__gprint_addComment("%s\n", comment);
}
//------------------------------------------------------------------------------
// GPRINT_ADDFILE
//------------------------------------------------------------------------------
void gprint_addFile(size_t level, bool last, char *object, char *comment) {
	__gprint_addIndent(level, last);
	__gprint_add("%s\n", object);
	__gprint_addComment("%s\n", comment);
}
//------------------------------------------------------------------------------
// GPRINT_CLOSE
//------------------------------------------------------------------------------
void gprint_close(FILE *fdout) {
	size_t n, m = 0;
	char line[2048], comment[MAX_DESCR + 1];
	unsigned char indent[2048];

	rewind(__fdfile);
	rewind(__fdcomment);
	while ((fgets(line, sizeof(line), __fdfile)) != NULL) {
		if (fgets(comment, sizeof(comment), __fdcomment) == NULL) {
			fatal("%s", "Incoherent temporary file contents");
		}
		comment[strlen(comment) - 1] = '\0';
		n = strlen(line);
		if (line[n - 1] == '\n') {
			line[n - 1] = '\0';
			--n;
		}
		size_t i, tobesubstrated, ctobesubstrated;
		char *ptmp;
		boolean ongoing = FALSE;
		switch (__terminalType) {
		case CMDWIN:		// Windows command line and Windows/Bash
			strcpy((char *) indent, line);
			i = strlen((char *) indent);
			while (i != 0) {
				if (indent[i] == SVBR || indent[i] == SV || indent[i] == SH || indent[i] == SLBC) {
					indent[i + 1] = '\0';
					break;
				}
				--i;
			}
			m = strlen((char *) indent);
			for (i = 0; i < m; i++) {
				if (indent[i] == SVBR) indent[i] = SV;
				if (indent[i] == SH || indent[i] == SLBC) indent[i] = ' ';
			}
			break;
		case XTERM:			// Linux (XTERM)
		case XTERMWIN:		// and MinGW shell
			tobesubstrated = ctobesubstrated = 0;
			strcpy((char *) indent, line);
			for (i = 0; i < n; i++) {
				ptmp = line + i;
				if (strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) {
					tobesubstrated += strlen(XGRAPHMODEON);
					ctobesubstrated += strlen(XGRAPHMODEON);
					ongoing = TRUE;
				} else {
					if (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0) {
						tobesubstrated += strlen(XGRAPHMODEOFF);
						ctobesubstrated += strlen(XGRAPHMODEOFF);
						indent[i + strlen(XGRAPHMODEOFF)] = '\0';
						ongoing = FALSE;
					} else {
						if (*(unsigned char *)ptmp == 195) tobesubstrated += 1;
						if (ongoing) {
							if (*ptmp == XSVBR) indent[i] = XSV;
							if (*ptmp == XSH || *ptmp == XSLBC) indent[i] = ' ';
						}
					}
				}
			}
			n -= tobesubstrated;
			m = strlen((char *) indent) - ctobesubstrated;
			break;
		}
		__gprint_line(fdout, n, line, comment, m, indent);
	}
	(void) fclose(__fdfile);
	(void) fclose(__fdcomment);
	(void)unlink(tmpnameFile);
	(void)unlink(tmpnameComment);
	__fdfile = NULL;
	__fdcomment = NULL;
}
///------------------------------------------------------------------------------
// GPRINT_ABORT
//------------------------------------------------------------------------------
void gprint_abort() {
	if (__fdfile != NULL) (void) fclose(__fdfile);
	if (__fdcomment != NULL) (void) fclose(__fdcomment);
	(void)unlink(tmpnameFile);
	(void)unlink(tmpnameComment);
	__fdfile = NULL;
	__fdcomment = NULL;
}
//------------------------------------------------------------------------------
