///------------------------------------------------------------------------------
// Copyright (c) 2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaTree
// Yet Another Tree - list contents of directories in a tree-like format
//------------------------------------------------------------------------------
#ifndef COMMON_H
#define COMMON_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define error(fmt, ...) \
	do { fprintf(stderr, "ERROR: " fmt "\n", __VA_ARGS__); } while (0)

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#define MAX_DESCR			1024
#define CONTINUATION_LINE	"\b"
//------------------------------------------------------------------------------
// TYPEDEF
//------------------------------------------------------------------------------
typedef enum { FALSE, TRUE } boolean;

#endif	// COMMON_H
//------------------------------------------------------------------------------
