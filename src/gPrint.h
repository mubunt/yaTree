//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: yaTree
// Yet Another Tree - list contents of directories in a tree-like format
//------------------------------------------------------------------------------
#ifndef GPRINT_H
#define GPRINT_H
//------------------------------------------------------------------------------
// EXTERNAL FUNCTIONS
//------------------------------------------------------------------------------
extern void	gprint_ini();
extern void	gprint_close(FILE *);
extern void gprint_abort();
extern void gprint_addDir(size_t, bool, char *, char *);
extern void gprint_addFile(size_t, bool, char *, char *);

#endif	// GPRINT_H
//------------------------------------------------------------------------------
