# *yaTree*, Yet Another Tree - list contents of directories in a tree-like format
***
**yaTree**  is a recursive directory listing program that produces a depth indented listing of files, With no  arguments,  tree  lists  the files in the current directory.  When directory argument is given, **yaTree** lists all the files and/or directories found in the given directory.


**yaTree** looks for a *.comment* file in a directory and append the text from that file to the output of the files and directories.


Upon  completion  of listing all files/directories found, **yaTree** returns the total number of files (regular or not) and/or directories listed.


**What differences with the **tree** package from *Steve Baker (ice@mama.indstate.edu*)?**
   - With **tree**, directory names and files are displayed in alphabetical order. With **yaTree**, the directory names are displayed in alphabetical order followed by the filenames in alphabetical order.
   - The names of the directories are followed by the character '/'.
   - If a file is not a regular file, then its type is indicated between "[]": character device, block device, FIFO, symbolic link, socket.
   - A comment area initiated by the character '#' is positioned at the end of each displayed line  (out of final report) and aligned with the longest of these lines.
   - For each dorectory and file, this comment area is filled with text coming from *.comment* file located in the current directory.
	
**What is the purpose of this application?**

Just to help me write the documentation of the applications I develop. See section "STRUCTURE OF THE APPLICATION".


**Example 1:** On Linux with *terminator* as terminal.

![yaTree](README_images/xubuntu-terminator-1.png  "yaTree")

**Example 2:** On Linux with *terminator* as terminal.

![yaTree](README_images/xubuntu-terminator-2.png  "yaTree")

**Example 3:** On Linux with *terminator* as terminal.

![yaTree](README_images/xubuntu-terminator-3.png  "yaTree")

**Example 4:** On Windows with *cmd*.

![yaTree](README_images/windows-cmd.png  "yaTree")
## .comment FILE
The syntax for each line in a file is as follows:

	[directory or file name] (space or tab)* [comment]

- directory or file name: name of a sub- directory or file (including suffix). If this field is empty (that means that the line starts with a space or a tab), the comment is considered as a continuation of the previous comment.
- space or tab: one space or tab atleast to separate name and comment.
- comment: free text up to 1024 characters.


**Example with multi-line comment:** 
![yaTree](README_images/xubuntu-terminator-4.png  "yaTree")


In this phase of generating or updating .comment files, you can use the utility [yaComment](https://mubunt.github.io/yaComment/). This tool  generates '.comment' files for the current tree structure and a standard description is proposed for some remarkable files. Directories _.git_, _bin_, _linux_, _LINUX_, _windows_ and _WINDOWS_ are ignored.
## LICENSE
**yaTree** is covered by the GNU General Public License (GPL) version 2 and above.
## USAGE
``` bash
$ yaTree --help
yaTree - Copyright (c) 2017, Michel RIZZO. All Rights Reserved.
yaTree - Version 1.1.0

Yet Another Tree - list contents of directories in a tree-like format

Usage: yaTree [OPTIONS]... [DIRECTORY]

  -h, --help           Print help and exit
  -V, --version        Print version and exit
  -v, --verbose        Verbose mode.  (default=off)
  -a, --all            Do not ignore entries starting with '.'.  (default=off)
  -d, --directory      List directories only.  (default=off)
  -n, --noreport       Turn off file/directory count at end of tree listing.
                         (default=off)
  -N, --nodescription  Turn off description display.  (default=off)

Exit: returns a non-zero status if an error is detected.

$ 
```
By default, DIRECTORY is ".".
## STRUCTURE OF THE APPLICATION
This section walks you through **yaTree**'s structure. Once you understand this structure, you will easily find your way around in **yaTree**'s code base.
``` bash
$ yaTree
./                               # Application level
├── README_images/               # Images for documentation
│   ├── windows-cmd.png          # -- Screenshot yaTree on Windows/cmd
│   ├── xubuntu-terminator-1.png # -- Screenshot yaTree on Ubuntu/terminator
│   ├── xubuntu-terminator-2.png # -- Screenshot yaTree on Ubuntu/terminator
│   ├── xubuntu-terminator-3.png # -- Screenshot yaTree on Ubuntu/terminator
│   └── xubuntu-terminator-4.png # -- Screenshot yaTree on Ubuntu/terminator
├── src/                         # Source directory
│   ├── Makefile                 # -- Makefile
│   ├── common.h                 # -- C Header file with data common to all '.c' files
│   ├── gPrint.c                 # -- C source file to manage display
│   ├── gPrint.h                 # -- C Header file with 'gPrint.c' function prototypes
│   ├── yaTree.c                 # -- C main source file
│   └── yaTree.ggo               # -- 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md                   # GNU General Public License markdown file
├── LICENSE.md                   # License markdown file
├── Makefile                     # Top-level makefile
├── README.md                    # ReadMe Mark-Down file
├── RELEASENOTES.md              # Release Notes markdown file
└── VERSION                      # Version identification text file

2 directories, 17 files
$
```

## HOW TO BUILD THIS APPLICATION
``` bash
	$ cd yaTree
	$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
``` bash
$ cd yaTree
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## SOFTWARE REQUIREMENTS
- For usage:
   - Nothing particular...
- For development:
  - *GengetOpt* binary package installed, version 2.22.6.
- Application developed and tested with XUBUNTU 17.10

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .
***
